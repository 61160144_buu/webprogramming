const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const mysql = require('mysql');
const alert = require('alert');
const { response } = require('express');
const { format } = require('morgan');
let app = express();
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static('html'));
app.use(express.static(__dirname));
app.use('/images', express.static(__dirname + '/img'));
const date = require('date-and-time');
const con = mysql.createConnection({
  host: 'localhost',
  user: "root",
  password: "",
  database: "dcoffee"
});
con.connect((err) => {
  if (err) {
    console.log('Error connecting to Db');
    return;
  }
  console.log('Connection connecting to Db');
});

app.get('/', function (req, res) {
  res.sendFile(__dirname + "/html/" + "login.html");
});
var count = "";
var id = "";
var name = "";
var lastname = "";
var position;
var salary;
var total_sale;
var datetimelogin;
var datetimelogout = "";
var datetime_logout = "";
var email
var password
app.post('/login', function (req, res) {
  email = req.body.email
  password = req.body.password
  con.query('SELECT * FROM mst_security s, mst_employee e WHERE USER = ? AND PASSWORD = ? AND s.id_employee = e.id_employee',
    [email, password], (err, result) => {
      if (result.length > 0) {
        //เป็นสมาชิก
        count = 0
        id = JSON.stringify(result[0].id_employee).replace(/"(.*)"$/, '$1')
        name = JSON.stringify(result[0].name).replace(/"(.*)"$/, '$1')
        lastname = JSON.stringify(result[0].surname).replace(/"(.*)"$/, '$1')
        position = JSON.stringify(result[0].position).replace(/"(.*)"$/, '$1')
        salary = JSON.stringify(result[0].salary).replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",")
        total_sale = JSON.stringify(result[0].total_sale).replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",")
        //บันทึกเวลาlogin ลง database
        const now = new Date();
        const timein = { datetime_login: date.format(now, 'YYYY-MM-DD HH:mm:ss'), id_employee: result[0].id_employee };
        con.query('INSERT INTO trn_login SET ?', timein, (err, row) => {
          if (err) throw err;
        });
        datetimelogin = JSON.stringify(timein.datetime_login).replace(/"(.*)"$/, '$1')
        // checkPosition
        if (position === "เจ้าของร้าน") {
          return res.redirect("/main")
        } else if (position === "พนักงาน") {
          return res.redirect("/mainemp")
        }
      } else {
        //คุณไม่เป็นสมาชิก
        alert('คุณไม่เป็นสมาชิก')
        return res.redirect('/')
      }
    });
  //หาวันที่logoutล่าสุด
  con.query('SELECT * FROM trn_logout', (err, rows) => {
    rows.forEach((row) => {
      if (row.id_employee === id) {
        datetime_logout = row.datetime_logout.toLocaleString('en-CA', { hour12: false });
      }
    });
    console.log(datetime_logout)
    if (datetime_logout === "") {
      datetimelogout = " ";
    } else {
      datetimelogout = JSON.stringify(datetime_logout).replace(/,/g,'').replace(/p.m./g,'').replace(/"(.*)"$/, '$1')
    }
  });
  //นับจำนวนเข้าสู่ระบบ
  con.query('SELECT * FROM trn_login', (err, rows) => {
    if (err) throw err;
    rows.forEach((row) => {
      if (row.id_employee === id) {
        count++;
      }
    });
  });
});

//หน้าหลักพนักงาน
app.get("/mainemp", function (req, res) {
  return res.render(__dirname + "/html/mainemp.html",
    {
      name: name,
      lastname: lastname
    });
})
//หน้าหลักเจ้าของร้าน
app.get("/main", function (req, res) {
  con.query('SELECT * FROM mst_employee WHERE mst_employee.id_employee = ?', [id], (err, rows) => {
    if (err) console.log(err);
    rows.forEach((row) => {
      id_employee = row.id_employee;
      name = row.name;
      lastname = row.surname;
    });
  return res.render(__dirname + "/html/main.html",
    {
      name: name,
      lastname: lastname
    });
})
})
//หน้าข้อมูลสมาชิก
app.get('/member', (req, res) => {
  con.query('SELECT * FROM  mst_employee e WHERE id_employee = ?',
    [id], (err, result) => {
      id = JSON.stringify(result[0].id_employee).replace(/"(.*)"$/, '$1')
      name = JSON.stringify(result[0].name).replace(/"(.*)"$/, '$1')
      lastname = JSON.stringify(result[0].surname).replace(/"(.*)"$/, '$1')
      position = JSON.stringify(result[0].position).replace(/"(.*)"$/, '$1')
      salary = JSON.stringify(result[0].salary).replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",")
      total_sale = JSON.stringify(result[0].total_sale).replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",")
  return res.render(__dirname + "/html/" + "member.html",
    {
      name: name,
      lastname: lastname,
      id: id,
      position: position,
      salary: salary,
      totalsalary: total_sale,
      login: datetimelogin,
      logout: datetimelogout,
      count: count
    });
})
})
//บันทึกเวลาlogout ลง database
app.get("/logout", (req, res) => {
  const now = new Date();
  const timeout = { datetime_logout: date.format(now, 'YYYY-MM-DD HH:mm:ss'), id_employee: id };
  con.query('INSERT INTO trn_logout SET ?', timeout, (err, result) => {
    return res.redirect('/')
  });
});
//หน้าจัดการผู้ใช้
app.get('/userIndex', (req, res) => {
  con.query("SELECT * FROM mst_employee", (err, rows) => {
    if (err) throw err;
    for (i = 0; i < rows.length; i++) {
      console.log('Search succesful')
    }
    return res.render(__dirname + "/html/user_index.html", {
      title: 'การจัดการข้อมูลผู้ใช้',
      users: rows
    });
  });
});
app.get("/adduser", function (req, res) {
  res.render(__dirname + "/html/" + "adduser.html")
})
//หน้าเพิ่มผู้ใช้
app.post('/add', (req, res) => {
  const name = req.body.fname;
  const lname = req.body.lname;
  const position = req.body.position;
  const salary = req.body.salary;
  const totalsalary = req.body.totalsalary;
  const user = req.body.email;
  const password = req.body.password;
  const employee = {
    name: name,
    surname: lname,
    position: position,
    salary: salary,
    total_sale: totalsalary
  }
  con.query('INSERT INTO mst_employee SET ?', employee, (err, int) => {
    if (err) throw err;
    console.log('Last insert ID employee :', int.insertId)
    security = {
      user: user,
      password: password,
      id_employee: int.insertId
    }
    con.query('INSERT INTO mst_security SET ? ', security, (err) => {
      if (err) throw err;
    });
  });
  res.redirect('/main')
});
//ค้นหาสมาชิก
app.get('/search/:word', function (req, res) {
  var { word } = req.params
  con.query('SELECT * FROM mst_employee WHERE name LIKE "%' + word + '%" OR surname LIKE "%' + word + '%" OR position LIKE "%' + word + '%" OR salary LIKE "%' + word + '%" OR total_sale LIKE "%' + word + '%"',
    function (err, rows) {
      if (err) throw err;
      for (i = 0; i < rows.length; i++) {
        console.log('Search succesful')
      }
      return res.render(__dirname + "/html/user_index.html", {
        title: 'การจัดการข้อมูลผู้ใช้',
        users: rows
      })
    });
})
//แก้ไขข้อมูลผู้ใช้แสดงข้อมูลที่จะแก้ไข
app.get('/edit/:userId',(req,res) => {
  var { userId } = req.params;
  con.query("SELECT mst_employee.id_employee, name, surname, position, salary, total_sale,user,password FROM mst_employee JOIN mst_security ON mst_employee.id_employee = mst_security.id_employee WHERE mst_employee.id_employee = ?  ",[userId],(err, result) => {
    if(err) throw err;
    res.render(__dirname + "/html/edituser.html",
    {
      user : result[0]
    })
  })  
})
//อัดเดตข้อมูลตาราง
app.post('/update',(req, res) => {
  const userId  = req.body.idemp;
  console.log(userId);
  con.query("UPDATE mst_employee SET name = '"+req.body.fname+"',surname= '"+req.body.lname+"',position= '"+req.body.position+"',salary= '"+req.body.salary.replace(/,/g,'')+"',total_sale= '"+req.body.totalsalary.replace(/,/g,'')+"' WHERE mst_employee.id_employee = '"+req.body.idemp+"'",(err, results) => {
    if(err) throw err;
  });
  con.query("UPDATE mst_security SET user= '"+req.body.email+"',password = '"+req.body.password+"' WHERE mst_security.id_security = '"+req.body.idemp+"'",(err, results) => {
    if(err) throw err;
  });
  res.redirect('/main');
});
//ลบข้อมูลผู้ใช้
app.get('/showdelete/:userId',(req,res) => {
  var { userId } = req.params;
  con.query("SELECT * FROM mst_employee WHERE id_employee = ?  ",[userId],(err, result) => {
    if(err) throw err;
    res.render(__dirname + "/html/deleteuser.html",{
      user : result[0]
    })
  })  
})
app.get('/delete/:userId',(req,res) => {
  var { userId } = req.params;
  con.query("DELETE t1,t2  FROM mst_employee t1 JOIN mst_security t2 ON t1.id_employee = t2.id_security  WHERE t1.id_employee = ?",[userId],(err, result) => {
    if(err) throw err;
    console.log('Delete successfull',result.affectedRows)
  })
  con.query("DELETE FROM trn_login WHERE id_employee = ?",[userId],(err, result) => {
    if(err) throw err;
    console.log('Delete successfull',result.affectedRows)
  })
  con.query("DELETE FROM trn_logout WHERE id_employee = ?",[userId],(err, result) => {
    if(err) throw err;
    console.log('Delete successfull',result.affectedRows)
  })
  res.redirect('/main');
})
app.listen(3000, '127.0.0.1', function () {
  console.log('Server Listen at http://127.0.0.1:3000');
});